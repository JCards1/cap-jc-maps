# @jcards/cap-jc-maps

A custom Google Maps component specifically for use with Jcards
This repository is utilised for a core function in the Jcards app.

## Usage

import {CapacityGoogleMaps} from "@jcards/cap-jc-maps"

## Contributors

- Maximus Agency Pty Ltd
- BlueSky Digital Labs


  Pod::Spec.new do |s|
    s.name = 'JcardsCapJcMaps'
    s.version = '0.0.5'
    s.summary = 'Plugin using native Maps API for Android and iOS.'
    s.license = 'MIT'
    s.homepage = 'https://bitbucket.org/JCards1/cap-jc-maps/'
    s.author = 'BlueSky Digital Labs'
    s.source = { :git => 'https://bitbucket.org/JCards1/cap-jc-maps/', :tag => s.version.to_s }
    s.source_files = 'ios/Plugin/**/*.{swift,h,m,c,cc,mm,cpp}'
    s.ios.deployment_target  = '12.0'
    s.dependency 'Capacitor'
    s.dependency 'GoogleMaps'
    s.dependency 'AnimatePolyline'
    s.static_framework = true
  end
